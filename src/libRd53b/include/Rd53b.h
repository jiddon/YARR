#ifndef RD53B_H
#define RD53B_H
// #################################
// # Author: Timon Heim
// # Email: timon.heim at cern.ch
// # Project: Yarr
// # Description: RD53B Library
// # Comment: Combines ITkPixV1 and CROCv1
// # Date: May 2020
// ################################

#include "FrontEnd.h"
#include "TxCore.h"
#include "RxCore.h"

#include "Rd53bCmd.h"
#include "Rd53bCfg.h"
#include "itkpix_efuse_codec.h" // EfuseData

class Rd53b : public FrontEnd, public Rd53bCfg, public Rd53bCmd{
    public:

        Rd53b();
        
        void init(HwController *arg_core, const FrontEndConnectivity& fe_cfg) override;
        void makeGlobal() override {m_chipId = 16;}
        std::unique_ptr<FrontEnd> getGlobal() override {
            return std::make_unique<Rd53b>();
        }

        void resetAllHard() override;
        void resetAllSoft() override;
        void configure() override;
        void configureInit();
        void configureGlobal();
        void configurePixels();
        void configurePixels(std::vector<std::pair<unsigned, unsigned>> &pixels);
        void configurePixelMaskParallel();
        
        yarrStatus checkCom() override;
        yarrStatus hasValidName() override;

        // Memory and chip register operation based on object
        yarrStatus writeRegister(Rd53bRegDefault Rd53bGlobalCfg::*ref, const uint16_t value);
        yarrStatus readRegister(Rd53bRegDefault Rd53bGlobalCfg::*ref, uint16_t &value, uint8_t &chipId);
        yarrStatus readRegister(Rd53bRegDefault Rd53bGlobalCfg::*ref, uint16_t &value);
        yarrStatus readUpdateWriteRegister(Rd53bRegDefault Rd53bGlobalCfg::*ref, const uint16_t value);
        
        // Memory only register operations
        yarrStatus getNamedRegister(std::string name, uint16_t &value) override;
        yarrStatus setNamedRegister(std::string name, const uint16_t value) override;
        
        // Memory and chip register operations based on name
        yarrStatus writeNamedRegister(std::string name, const uint16_t value) override;
        yarrStatus readNamedRegister(std::string name, uint16_t &value) override;
        yarrStatus readUpdateWriteNamedRegister(std::string name, const uint16_t value) override;

        Rd53bRegDefault Rd53bGlobalCfg::* getNamedRegisterObject(std::string name);

        void setInjCharge(double charge, bool sCap=true, bool lCap=true) override {
            this->writeRegister((Rd53bRegDefault Rd53bGlobalCfg::*)&Rd53bGlobalCfg::InjVcalDiff, this->toVcal(charge));
        }
        
        static std::pair<uint32_t, uint32_t> decodeSingleRegRead(uint32_t higher, uint32_t lower);
        static std::tuple<uint8_t, uint32_t, uint32_t> decodeSingleRegReadID(uint32_t higher, uint32_t lower);

        
        // perform the necessary steps to program the E-fuse circuitry and perform
        // the readback of the E-fuse data
        itkpix_efuse_codec::EfuseData readEfuses();
        uint32_t readEfusesRaw();
        uint32_t getEfuses();
        uint8_t readChipId();

        void runRingOsc(uint16_t duration, bool isBankB);
        void confAdc(uint16_t MONMUX, bool doCur = false) override;
    protected:
    private:
};

#endif
