# Commercial Hardware Guide

## DAQ PC

ASUS mainboards are strongly recommended.

PCIe x4 gen 2 or newer have to be supported.

Not working are AMD systems and most Dell or HP computers.

Some example setups that have been tested working and in use:

- [a more recent build](https://uk.pcpartpicker.com/list/DVkkyg)
- [System 1](https://pcpartpicker.com/list/ZVmZRk)
- [System 2](https://pcpartpicker.com/list/m7ZhrV)
- [Mini version](https://pcpartpicker.com/list/MrXM3F)

### Example Main Components

#### A more recent build (2024)
- CPU: Intel Core i5-14600K 3.5 GHz 14-Core Processor  (£289.99 @ Amazon UK)
- CPU Cooler: Noctua NH-D15S 82.52 CFM CPU Cooler  (£89.95 @ Amazon UK)
- Motherboard: Asus PRIME H770-PLUS ATX LGA1700 Motherboard  (£194.99 @ AWD-IT)
- Memory: Corsair Vengeance RGB 32 GB (2 x 16 GB) DDR5-5600 CL36 Memory  (£114.00 @ Amazon UK)
- Storage: Kingston NV2 1 TB M.2-2280 PCIe 4.0 X4 NVME Solid State Drive  (£57.99 @ Ebuyer)
- Case: Fractal Design Pop Air ATX Mid Tower Case  (£76.60 @ CCL Computers)
- Power Supply: be quiet! System Power 10 850 W 80+ Gold Certified ATX Power Supply  (£86.15 @ NeoComputers)
- Total: £909.67

#### An older example
- Motherboard ASUS 90MB0WG0-M0EAY0 (140 CHF)
- CPU Intel Core i5-8400 (6X2.8) BOXED 1151v2 (200 CHF)
- CPU fan NH-L9I (50 CHF)
- Samsung 970 Evo Plus MZ-V7S500BW
- Power supply Corsair CP-9020182-EU (140 CHF)
- RAM memory CORSAIR VENGEANCE LPX 16Go 1x16
- Hard disk Western Digital WD4005FZBX 4TB (180 CHF)
- Case Lian Li PC-O11DX 


## DisplayPort cables
DisplayPort (DP) cables used for readout should be rated to v1.2
or better.
Below is a list with recommended cables:

- Cables by [StarTech](https://www.startech.com/en-us/cables/mdp2dpmm2mw) have been shown to work best.
- [Tripp Lite cable](https://www.digikey.com/en/products/detail/tripp-lite/P583-006-BK/7696233)
- For long cables, SLAC recommends this [BENFEI 15ft cable](https://www.amazon.com/gp/product/B07Z4SQVXP/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&th=1), through a [systematic survey](https://indico.cern.ch/event/1012474/contributions/4248883/attachments/2197664/3715968/Cable_SignalLoss_Feb_2021.pdf) and [direct comparison with previously favored 4m StarTech cable](https://indico.cern.ch/event/1001271/contributions/4204945/attachments/2177877/3679557/minDP_cable_19_1_demo_Jan21_2021.pdf).
